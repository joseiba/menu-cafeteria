from djongo import models

class Pedido(models.Model):
    mesa=models.IntegerField(null=True)
    total=models.IntegerField(default=0)
    cliente = models.CharField(max_length=100, null=True)
    estado=models.IntegerField(default=0)
    lista_productos = models.JSONField(null=True)