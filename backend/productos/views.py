from rest_framework import viewsets
from .models import Producto
from .serializers import ProductoSerializer
from cafeteria_be.permissions import IsRecepcionista, IsAdminOrIsRecepcionista, IsAdmin

class ProductosViewSet(viewsets.ModelViewSet):
    # Minimamente hay que pasar queryset y serializer_class
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer
    def get_permissions(self):
        permission_classes = []
        if self.action == 'retrieve' or self.action == 'list':
            permission_classes = [IsAdminOrIsRecepcionista]
        elif self.action == 'update' or self.action == 'partial_update' or self.action == 'destroy':
            permission_classes = [IsAdminOrIsRecepcionista]
        elif self.action == 'create':
            permission_classes = [IsAdmin]
        elif self.action == 'productos': # Endpoint custom
            permission_classes = [IsRecepcionista]        
        return [permission() for permission in permission_classes]