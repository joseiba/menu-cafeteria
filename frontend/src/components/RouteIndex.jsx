import '../App.css';
import Container from './Dashboard';
import Sidebar from './SideBar';
import AddProducts from './AddProducts';
import Pedidos from './Pedidos';
import Usuarios from './Usuarios';
import AddUser from './AddUser';
import EditUser from './EditUser';
import Cocina from './Cocina';
import EditProduct from './EditProduct';

import { BrowserRouter, Routes, Route } from "react-router-dom";

function RouteIndex({ setUserId }) {
  const role = localStorage.getItem("role")
  return (
    <div className="App">
      <BrowserRouter>
        <Sidebar setUserId={setUserId} />
        <Routes>

          <Route>
            <Route path="/" element={<Container />} exact />
            <Route path="/pedidos" element={<Pedidos />} />


            <Route path="/cocina" element={<Cocina />} />

            <Route path="/agregarProducto" element={<AddProducts />} />
            {/* <Route  path="/usuarios" element={<Usuarios />} /> */}
            {/* <Route  path="/agregarUsuario" element={<AddUser />} />
              <Route  path="/editarUsuario" element={<EditUser />} /> */}
            <Route path="/editarProducto" element={<EditProduct />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default RouteIndex;