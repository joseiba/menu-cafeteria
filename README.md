# menu-cafeteria



## Getting started

Esta aplicacion web, es un ejemplo de un menu de cafeteria simple.

## Clonar el Repositorio

- Debe clonar primeramente el proyecto: 

```
git clone https://gitlab.com/joseiba/turnero.git
```
## Instalacion Backend

Para instalar los requerimientos primero debe tener python instalado en su equipo

Luego de instalarlo, debe abrir la linea de comando o dentro del vsCode, en la terminal, acceder dentro de la carpeta backend donde contiene el archico requirements.txt:

```
pip install -r requirements.txt
```

Luego ejecutar el siguiente comando: 

```
python manage.py makemigrations
```

Despues el comando:

```
python manage.py migrate
```
Debe crear un super usuario con el siguiente comando:

```
python manage.py createsuperuser
```

Luego ya puede levantar el servido con el comando:

```
python manage.py runserver
```

## Instalacion Frontend
Para instalar los node_modules, primero debe tener node instalado en su equipo.

Luego ingresar en la carpeta del frontend, y ejecutar el siguiente comando: 

```
npm install
```
Y luego debe ejecutar el siguiente comando para levantar el servidor:

```
npm run dev
```

## Importante
Este proyecto esta hecho con la base de datos no relacional MongoDb, para utilizarla primero debe tener instalado MongoDb.
